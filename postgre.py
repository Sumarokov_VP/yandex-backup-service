from datetime import datetime
import shutil
from subprocess import PIPE,Popen
import os
from settings import Settings
from log_worker import LogWorker
from gpg import encrypt_file



def backup(logger):
    db_server = Settings().data['postgres']
    now: datetime = datetime.now()
    username = db_server['pguser']
    password = db_server['pgpassword']
    db_host = db_server['host']
    out_path = db_server['backup_path']
    archive = db_server['archive']
    encode = db_server['encode']

    os.makedirs(
        name= out_path,
        exist_ok= True,
    )

    for db in db_server['dbs']:
        db_name = db['name'] 
        base_name = f'{db_name}_{now.year}{now.month}{now.day}_{now.hour}{now.minute}{now.second}'
        fn = base_name + '.sql'
        dir_path = os.path.join(out_path,base_name)
        os.makedirs(
            dir_path,
            exist_ok=True
        )
        full_path = os.path.join(dir_path,fn)

        #with gzip.open(full_path, 'wb') as f:
        #    pg_dump('-h', 'localhost', '-U', 'sumarokov', db.name, _out=f)
        command =   f'pg_dump -h {db_host} {db_name} > {full_path}'
        logger.log(command)
        proc = Popen(
                command,
                shell=True,
                stdin=PIPE,
                stdout=PIPE,
                stderr=PIPE,
                env={
                    'PGUSER': username,
                    'PGPASSWORD': password
                }
        )
        #return p.communicate('{}\n'.format(database_password))
        proc.wait()

        if archive:
            archive_name = os.path.join(out_path, base_name)
            shutil.make_archive(
                base_name= archive_name,
                format= 'zip',
                root_dir= dir_path,
            )
            shutil.rmtree(dir_path)
            if encode:
                encrypt_file(archive_name+'.zip')
        elif encode:
            encrypt_file(full_path)
        

if __name__ == "__main__":
    log = LogWorker()
    backup(logger=log)