import gnupg
import os
from settings import Settings

def encrypt_file(filename, delete_original = True):
    settings = Settings().data['gpg']
    gpg = gnupg.GPG(gnupghome= settings['home_dir'])
    with open(filename, 'rb') as file:
        status = gpg.encrypt_file(
            file= file,
            recipients=settings['encode_keys'],
            output= filename+ '.gpg',
            armor= False,
        )
    if delete_original:
        os.remove(filename)

if __name__ == "__main__":
    encrypt_file('test.txt')