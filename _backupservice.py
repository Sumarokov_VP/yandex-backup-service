import os
import traceback

from signal import signal, SIGPIPE, SIG_DFL # remove for windows
import datetime, pytz

from yandex_disk import YDWorker
from log_worker import LogWorker
from settings import Settings
import postgre
import ms_sql

try:
	l = LogWorker()
except Exception as e:
	print(f'LogWorker creation error: {str(e)}')
#try:
signal(SIGPIPE,SIG_DFL) # remove for windows
l.log('------------------------------Start------------------------------')
settings = Settings().data

try:
    yw = YDWorker(l)
    l.log(f'Yadisk instance created')
    s = format(yw.free_space(), '.2f')
    l.log(f'Free space: {s} Gb')

    directory_list = settings['yandex']['local_folders'] 
except Exception as e:
    l.log(f'Error in start block: {str(e)}')

l.log('------------------------------Cleaning------------------------------')
try:
    del_dest = settings['yandex']['delete_dest_older_than']
    if not del_dest == 0:
            duedate = datetime.datetime.utcnow().replace(tzinfo=pytz.utc) - datetime.timedelta(days = del_dest)
            dest_folder = settings['yandex']['destination_folder']
            l.log(f'Deleting files created before {duedate} from YandexDisk:{dest_folder} ...')
            yw.clean(dest_folder)
    else:
            l.log(f'Destination folder cleaning disabled')

    del_source = settings['yandex']['delete_source_older_than']
    if not del_source == 0:
            duedate = datetime.datetime.utcnow().replace(tzinfo=pytz.utc) - datetime.timedelta(days = del_source)
            dest_folder = settings['yandex']['destination_folder']
            item: str
            for item in directory_list:
                    from_dir = item.rstrip('\n')
                    l.log(f'Deleting files created before {duedate} from {from_dir} ...')
                    for root, dirs, files in os.walk(from_dir):
                            for file in files:
                                    path = os.path.join(root, file)
                                    modify_date = datetime.datetime.fromtimestamp(os.path.getmtime(path)).replace(tzinfo=pytz.utc)
                                    if modify_date < duedate:
                                            try:
                                                    os.remove(path)
                                            except Exception as e:
                                                    l.log(f'Ошибка: {traceback.format_exc()}')
    else:
            l.log(f'Source folder cleaning disabled')
except Exception as e:
    l.log(f'Error in clean block: {str(e)}')

try:
    l.log('------------------------------PostgreSQL backup------------------------------')
    postgre.backup(l)
except Exception as e:
    l.log(f'Error in postgresql block: {str(e)}')

try:
    l.log('------------------------------MSSQL backup------------------------------')
    ms_sql.backup(l)
except Exception as e:
    l.log(f'Error in sql block: {str(e)}')


try:
    l.log('------------------------------Uploading------------------------------')
    record_counter = 0
    for item in directory_list:
            record_counter += 1
            l.log(f'Uploading {item}')
            from_dir = item.rstrip('\n')
            yw.recursive_upload(from_dir)

    l.log('--------------------------- Finished ---------------------------------')
except Exception as e:
    l.log(f'Error in upload block: {str(e)}')
#except Exception as e:
#	l.log(f'Global application error: {str(e)}')
