import logging
import os

FOLDER_NAME = 'logs'
LOG_FILENAME = os.path.join(FOLDER_NAME, 'service.log')


class LogWorker:

    def __init__(self):
        if not os.path.isdir(FOLDER_NAME): os.mkdir(FOLDER_NAME)
        self.logger = logging.getLogger()
        self.logger.setLevel(logging.INFO)
        handler = logging.FileHandler(LOG_FILENAME, 'w', 'utf-8')
        handler.setFormatter(
            logging.Formatter(u'%(filename)s[LINE:%(lineno)d]# %(levelname)-8s [%(asctime)s]  %(message)s'))
        self.logger.addHandler(handler)

    def log(self, text):
        print(text)
        self.logger.info(text)

l = LogWorker()