import pyodbc
from datetime import datetime
import os

from settings import Settings


def backup(log):
    db_server = Settings().data['mssql']
    now: datetime = datetime.now()
    con = pyodbc.connect(db_server['connection_string'], autocommit=True)
    for db in db_server['dbs']:
        db_name = db['name']
        if not db['split']:
            fn = f'{db_name}_{now.year}{now.month}{now.day}_{now.hour}{now.minute}{now.second}_sql.bak'
            full_path = os.path.join(db['backup_path'],fn)
            query = f"BACKUP DATABASE {db_name} TO DISK='{full_path}'"
        else:
            fn = f'{db_name}_{now.year}{now.month}{now.day}_{now.hour}{now.minute}{now.second}_sql'
            folder = os.path.join(db['backup_path'],fn)
            if not os.path.isdir(folder):
                os.mkdir(folder)
            fn = f'{db_name}_{0}.bak'
            full_path = os.path.join(folder, fn)
            query = f"BACKUP DATABASE {db_name} TO DISK='{full_path}'"
            for i in range(1, db['files_count']):
                fn = f'{db_name}_{i}.bak'
                full_path = os.path.join(folder, fn)
                query += f", DISK='{full_path}'"
            

        log.log(query)
        cur = con.cursor()
        cur.execute(query)
        while cur.nextset(): 
            pass
        cur.close()
    return