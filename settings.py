import json


class Settings:
    def __init__(self, file_name = 'settings.json'):
        self.file_name = file_name
        with open(file_name) as json_file:
            self.data = json.load(json_file)

    def save(self):
        with open(self.file_name, "w") as outfile:
            json.dump(self, outfile)