import posixpath
import datetime
import os
import sys
import webbrowser
import uuid
import requests
import yadisk
from yadisk import YaDisk
import pytz
from settings import Settings
#import log_worker

class YDWorker:
    def __init__(self, log):
        token = GetToken()
        self.y = YaDisk(token=token)
        if not(self.y.check_token()):
            token = NewToken()
            self.y = YaDisk(token=token)
        settings = Settings().data
        self.settings = settings['yandex']
        self.l = log
        self.yandex_root = self.settings['destination_folder']
        self.days_l = self.settings['delete_dest_older_than']

    def file_upload(self, source_file_path):
        to_dir = self.yandex_root + f'/Zipped'

        try: self.y.mkdir(self.yandex_root)
        except yadisk.exceptions.PathExistsError: pass    

        try: self.y.mkdir(to_dir)
        except yadisk.exceptions.PathExistsError: pass            



        dest_file_path = posixpath.join(to_dir, os.path.basename(source_file_path))

        #self.l.log(f'Uploading {source_file_path} to {dest_file_path}')
        try:
            self.y.upload(
                source_file_path,
                dest_file_path,
                overwrite = False,
                timeout= self.settings['upload_timeout'],
            )
        except yadisk.exceptions.PathExistsError: self.l.log(f'File {source_file_path} in directory {dest_file_path} already exists')
        except yadisk.exceptions.PathExistsError: self.l.log(f'Insufficient Storage')
        except Exception:
            e = sys.exc_info()[1]
            self.l.log(f'Error: {e.args[0]}')

    def recursive_upload(self, from_dir):
        if not self.y.exists(self.yandex_root):
            self.y.mkdir(self.yandex_root)
        
        dirname = os.path.basename(from_dir)
        to_dir = self.yandex_root + f'/{dirname}'

        walk = os.walk(from_dir)
        for root, dirs, files in walk:
            p = root.split(from_dir)[1].strip(os.path.sep)
            dest_folder = posixpath.join(to_dir, p)

            try:
                self.y.mkdir(dest_folder)
            except yadisk.exceptions.PathExistsError:
                pass

            files.sort()
            for file in files:
                path = os.path.join(root, file)
                dest_file_path = posixpath.join(dest_folder, file)
                p_sys = p.replace("/", os.path.sep)
                source_file_path = os.path.join(from_dir, p_sys, file)
                size = os.stat(path).st_size/1024/1024/1024
                freespace = self.free_space()
                root_size = self.folder_size(self.yandex_root)

                if size >= freespace and root_size > 0 and self.settings['delete_oldest_on_no_space']:
                    s = format(size, '.2f')
                    self.l.log(f'Not enough space for file: {path} size: {s} Gb')
                    self.l.log(f'Deleting oldest files')
                    self.clean_by_size(size)
                if size >= freespace and root_size == 0:
                    self.l.log(f'There is no enough space on the Yandex Disk to upload file {path}.'+
                    f' And folder {self.yandex_root} is empty. Uploading of this file is canceled')
                    continue

                self.l.log(f'Uploading {source_file_path}')
                try:
                    self.y.upload(
                        source_file_path,
                        dest_file_path,
                        overwrite = False,
                        timeout= self.settings['upload_timeout'],
                    )
                except yadisk.exceptions.PathExistsError: self.l.log(f'File already exists')
                except yadisk.exceptions.PathExistsError: self.l.log(f'Insufficient Storage')
                except Exception:
                    e = sys.exc_info()[1]
                    self.l.log(f'Error: {e.args[0]}')

    # Clean by days_live
    def clean(self, dir):
        try:
            self.y.mkdir(dir)
        except yadisk.exceptions.PathExistsError:
            pass
        dir_content = self.y.listdir(dir)
        duedate = datetime.datetime.utcnow().replace(tzinfo=pytz.utc) - datetime.timedelta(days = self.days_l)
        filescount = 0
        for item in dir_content:
            if item.type == 'dir':
                cnt = self.clean(item.path)
                if cnt==0:
                    self.y.remove(item.path, permanently = True)
                else: filescount +=1
            else:
                if item.created <= duedate:
                    s = format(self.free_space(), '.2f')
                    self.l.log(f'Deleting old file {item.path}. Created date:{item.created}')
                    self.y.remove(item.path, permanently = True)
                    self.l.log(f'Delete complete. Free space{s} Gb')
                else: filescount +=1
        return filescount

    # Clean oldest files by new file size
    def clean_by_size(self, new_file_size):
        while new_file_size >= self.free_space() and self.folder_size(self.yandex_root) > 0:
            d, p = self.find_oldest_file(self.yandex_root)
            try:
                self.l.log(f'Removing oldest file: {p} date creation: {d}')
                self.y.remove(p, permanently = True)
            except Exception:
                e = sys.exc_info()[1]
                self.l.log(f'Error: {e.args[0]}')
        s = format(self.free_space(), '.2f')
        self.l.log(f'Delete complete. Free space {s} Gb')

    def folder_size(self, dir_path):
        dir_content = self.y.listdir(dir_path)
        size = 0
        for item in dir_content:
            if item.type == 'dir':
                size += self.folder_size(item.path)
            else:
                size += item.size
        return size

    def free_space(self):
        info = self.y.get_disk_info()
        fs = (info.total_space - info.used_space)/1024/1024/1024
        return fs

    def find_oldest_file(self, root):
        dir_content = self.y.listdir(root)
        dt = datetime.datetime.utcnow().replace(tzinfo=pytz.utc)
        path = ''
        for item in dir_content:
            if item.type == 'dir':
                d, p = self.find_oldest_file(item.path)
                if d < dt and not p == '':
                    dt = d
                    path = p
            else:
                if item.created < dt:
                    dt = item.created
                    path = item.path
        return dt, path

def GetOAuthCode():
    settings = Settings().data
    authorization_base_url = 'https://oauth.yandex.ru/authorize'
    client_id = settings['yandex']['client_id']
    device_id = settings['yandex']['device_id']

    if device_id == '': #generate new device id
        settings['yandex']['device_id'] = str(uuid.uuid4())
        settings.save()
    auth_url = f'{authorization_base_url}?response_type=code&client_id={client_id}&device_id={device_id}'

    webbrowser.open_new(auth_url)
    print(auth_url)
    return input("Yandex code: ")

def GetOAuthToken(code):
    settings = Settings().data
    client_id = settings['yandex']['client_id']
    client_secret = settings['yandex']['client_secret']
    TOKEN_URL = "https://oauth.yandex.ru/token"
    device_id = settings['yandex']['device_id']

    if device_id == '': #generate new device id
        settings['yandex']['device_id'] = str(uuid.uuid4())
        settings.save()
    
    #REDIRECT_URI = "https://www.getpostman.com/oauth2/callback"
    #client_auth = requests.auth.HTTPBasicAuth(client_id, client_secret)
    post_headers = {"Content-Type": "application/x-www-form-urlencoded"}

        
    post_data = {"grant_type": "authorization_code",
                "code": code,
                'client_id': client_id,
                'client_secret': client_secret}
    response = requests.post(TOKEN_URL,
                            headers=post_headers,
                            data=post_data)
    token_json = response.json()

    token = str(token_json["access_token"])
    settings['yandex']['oauth_token'] = token
    settings.save()

    return token

def GetToken():
    settings = Settings().data
    token = settings['yandex']['oauth_token']
    if token == '':
        token = GetOAuthToken(GetOAuthCode())
    return token

def NewToken():
    settings = Settings().data
    token = GetOAuthToken(GetOAuthCode())
    settings['yandex']['oauth_token']  = token
    settings.save()
    return token

if __name__ == "__main__":
	NewToken()
